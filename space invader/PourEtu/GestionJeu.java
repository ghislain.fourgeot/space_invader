// CLASSE DONNEE AUX ETUDIANTS
// A COMPLETER


class GestionJeu{
	/* ======== Attributs ======== */
	Dessin dessin;
	Vaisseau joueur;
	// A COMPLETER

	/* ======== Constructeur ======== */
	// ATTENTION, seul le constructeur sans paramètre est accepté
	GestionJeu(){
		dessin = new Dessin();
		joueur = new Vaisseau(30.0);
		// A COMPLETE
	}

	/* ======== Getteurs ======== */
	int getLargeur(){ return 100; }
	int getHauteur(){ return 60; }


	// ATTENTION - la méthode getDessin() est appelée environ 40 fois par seconde
	// donc, il ne faut pas instancier de nouvel objet dans cette
	// méthode au risque de saturer rapidement la mémoire
	Dessin getDessin(){
		dessin.ajouteDessin(joueur.getRpz());
		// A COMPLETER
		return dessin;
	}

	/* ======== Autres méthodes ======== */


	// ATTENTION - la méthode jouerUnTour() est appelée environ 40 fois par seconde
	// donc, il ne faut pas instancier de nouvel objet dans cette
	// méthode au risque de saturer rapidement la mémoire
	void jouerUnTour(){
		// A COMPLETER
	}


	void toucheEspace(){
		// A COMPLETER
	}

	void toucheDroite(){
		joueur.deplaceX(1.0);
	}
	void toucheGauche(){
		joueur.deplaceX(-1.0);
	}

//nils.aburegaiba@univ-orleans.fr

}
