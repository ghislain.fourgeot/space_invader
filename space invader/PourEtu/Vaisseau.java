class Vaisseau {
  Double posX ;
  Dessin representation ;

   Vaisseau (Double x){
     this.posX = x;
     this.representation = new Dessin();
     this.representation.ajouteChaine(0,3,"      ▄");
     this.representation.ajouteChaine(0,2,"     ███");
     this.representation.ajouteChaine(0,1,"▄███████████▄");
     this.representation.ajouteChaine(0,0,"█████████████");

  }
  public Dessin getRpz(){
    return this.representation;
  }
  void deplaceX (double deplacementX){
    posX+=deplacementX;
    representation.deplacerX(deplacementX);
  }
}
